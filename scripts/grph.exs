[input_file|_rest] = System.argv
case File.read(input_file) do
  { :ok, text } ->
    Grph.print_adjacency(text, 3)
  { :error, :enoent } -> IO.puts "File not found"
end
