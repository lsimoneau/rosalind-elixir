[input_file|_rest] = System.argv
case File.read(input_file) do
  { :ok, text } ->
    fasta = Fasta.parse(text)
    IO.puts Cons.consensus(fasta)
    IO.puts Enum.map(Cons.profile(fasta), fn(seq) ->
      (elem(seq, 0) |> Atom.to_string |> String.upcase) <> ": " <>
        Enum.join(elem(seq, 1), " ")
    end) |> Enum.join("\n")
  { :error, :enoent } -> IO.puts "File not found"
end
