defmodule ProbTest do
  use ExUnit.Case

  test "calculating probabilities of a random DNA string for given G/C contents" do
    assert Prob.probabilities("ACGATACAA", "0.129 0.287 0.423 0.476 0.641 0.742 0.783") == "-5.737 -5.217 -5.263 -5.36 -5.958 -6.628 -7.009"
  end
end
