defmodule FibdTest do
  use ExUnit.Case

  test "it calculates the number of rabbits left after n months given a life expectancy" do
    assert Fibd.total_population(6, 3) == 4
  end
end
