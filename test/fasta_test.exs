defmodule FastaTest do
  use ExUnit.Case

  test "it converts a fasta string into a list of tuples" do
    assert Fasta.parse(
    """
      >Rosalind_1
      ATCCAGCT
      ATCCAGCT
      >Rosalind_2
      GGGCAACT
      ATCCAGCT
      AAGCAACC
      >Rosalind_3
      ATGGATCT
      >Rosalind_4
      AAGCAACC
      >Rosalind_5
      TTGGAACT
      >Rosalind_6
      ATGCCATT
      >Rosalind_7
      ATGGCACT
    """
    ) == [
      { "Rosalind_1", "ATCCAGCTATCCAGCT" },
      { "Rosalind_2", "GGGCAACTATCCAGCTAAGCAACC" },
      { "Rosalind_3", "ATGGATCT" },
      { "Rosalind_4", "AAGCAACC" },
      { "Rosalind_5", "TTGGAACT" },
      { "Rosalind_6", "ATGCCATT" },
      { "Rosalind_7", "ATGGCACT" },
    ]
  end
end
