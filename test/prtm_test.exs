defmodule PrtmTest do
  use ExUnit.Case

  test "calculating the mass of a protein" do
    assert Prtm.mass("SKADYEK") |> Float.round(3) == 821.392
  end
end
