defmodule ConsTest do
  use ExUnit.Case

  @fasta [
    { "Rosalind_1", "ATCCAGCT" },
    { "Rosalind_2", "GGGCAACT" },
    { "Rosalind_3", "ATGGATCT" },
    { "Rosalind_4", "AAGCAACC" },
    { "Rosalind_5", "TTGGAACT" },
    { "Rosalind_6", "ATGCCATT" },
    { "Rosalind_7", "ATGGCACT" }
  ]

  test "finding the profile of a set of dna sequences" do
    profile = Cons.profile(@fasta)
    assert profile[:a] == [ 5, 1, 0, 0, 5, 5, 0, 0 ]
    assert profile[:c] == [ 0, 0, 1, 4, 2, 0, 6, 1 ]
    assert profile[:g] == [ 1, 1, 6, 3, 0, 1, 0, 0 ]
    assert profile[:t] == [ 1, 5, 0, 0, 0, 1, 1, 6 ]
  end

  test "finding the consensus string of a set of dna sequences" do
    assert Cons.consensus(@fasta) == "ATGCAACT"
  end
end
