defmodule GrphTest do
  use ExUnit.Case

  @input """
  >Rosalind_0498
  AAATAAA
  >Rosalind_2391
  AAATTTT
  >Rosalind_2323
  TTTTCCC
  >Rosalind_0442
  AAATCCC
  >Rosalind_5013
  GGGTGGG
  """

  @output [
    { "Rosalind_0498", "Rosalind_2391" },
    { "Rosalind_0498", "Rosalind_0442" },
    { "Rosalind_2391", "Rosalind_2323" }
  ]


  test "the O3 adjacency graph of the provided set of dna strings" do
    assert Grph.adjacency(@input, 3) == @output
    Grph.print_adjacency(@input, 3)
  end
end
