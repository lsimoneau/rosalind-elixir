defmodule IevTest do
  use ExUnit.Case

  test "it calculates the expected number of offsprint with the dominant phenotype" do
    assert Iev.expected("1 0 0 1 0 1") == 3.5
  end
end
