defmodule Cons do
  @nucleotides [:a, :c, :g, :t]

  def consensus(fasta) do
    profile = profile(fasta)
    consensus_from_profile(profile, length(profile[:a]))
  end

  def consensus_from_profile(_, cons_string, 0) do
    cons_string
  end

  def consensus_from_profile(profile, cons_string \\ "", length) do
    next_char = consensus_for_head(profile)
    profile_tails = Enum.map(profile, fn(x) -> { elem(x, 0), elem(x, 1) |> tl } end)
    consensus_from_profile(profile_tails, cons_string <> next_char, length - 1)
  end

  defp consensus_for_head(profile) do
    Enum.max_by(profile, fn(x) -> hd(elem(x, 1)) end)
      |> elem(0)
      |> Atom.to_string
      |> String.upcase
  end

  def profile(fasta) do
    sequences = Enum.map fasta, fn(seq) -> elem(seq, 1) |> String.split("", trim: true) end
    count_nucleotides(sequences, [a: [], c: [], g: [], t: []], length(hd(sequences)))
  end

  defp count_nucleotides(_, profile, 0), do: profile
  defp count_nucleotides(sequences, profile, remaining) do
    new_profile = update_profile(profile, counts_for_head(sequences))

    sequence_tails = Enum.map(sequences, &(tl(&1)))
    count_nucleotides(sequence_tails, new_profile, remaining - 1)
  end

  defp update_profile(profile, counts) do
    Enum.reduce @nucleotides, profile, fn(letter, profile) ->
      value = counts[letter] || 0
      Dict.update(profile, letter, [value], &(List.insert_at(&1, -1, value)))
    end
  end

  defp counts_for_head(sequences) do
    Enum.reduce sequences, HashDict.new, fn(seq, acc) ->
      case hd(seq) do
        "A" -> Dict.update(acc, :a, 1, &(1 + &1))
        "C" -> Dict.update(acc, :c, 1, &(1 + &1))
        "G" -> Dict.update(acc, :g, 1, &(1 + &1))
        "T" -> Dict.update(acc, :t, 1, &(1 + &1))
      end
    end
  end
end
