defmodule Iev do
  def expected(input) do
    String.split(input)
    |> Enum.map(&(String.to_integer(&1)))
    |> Enum.zip([2, 2, 2, 1.5, 1.0, 0])
    |> Enum.map(fn({a,b}) -> a * b end)
    |> Enum.sum
  end
end
