defmodule Fasta do
  def parse(input) do
    String.strip(input)
      |> String.split(">", trim: true)
      |> parse_sequences
  end

  defp parse_sequences(sequences) do
    Enum.map sequences, fn(sequence) ->
      lines = String.split(sequence, ~r/\s/)
      { hd(lines), join(tl(lines)) }
    end
  end

  defp join(lines) do
    Enum.join(lines) |> String.strip
  end
end
