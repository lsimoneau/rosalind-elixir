defmodule Grph do
  def print_adjacency(input, o) do
    output = adjacency(input, o)
    Enum.each output, fn(edge) ->
      Tuple.to_list(edge) |> Enum.join(" ") |> IO.puts
    end
  end

  def adjacency(input, o) do
    fasta = Fasta.parse(input)
    find_overlaps(fasta, fasta, o, [])
  end

  defp find_overlaps([], _, _, output), do: output
  defp find_overlaps([current|remaining], all, o, output) do
    suffix = elem(current, 1) |> String.slice(-o..-1)
    overlaps = Enum.filter(all, fn(f) ->
      f != current and elem(f, 1) |> String.slice(0..o - 1) == suffix
    end)
    |> Enum.map(fn(f) -> { elem(current, 0), elem(f, 0) } end)
    find_overlaps(remaining, all, o, output ++ overlaps)
  end
end
