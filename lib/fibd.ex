defmodule Fibd do
  def total_population(months, max_age) do
    do_month([1], max_age, months)
  end

  def do_month(population, _, 1), do: Enum.sum(population)
  def do_month(population, max_age, months) do
    new_population = [babies(population)|population] |> Enum.take(max_age)
    do_month(new_population, max_age, months - 1)
  end

  defp babies([_|adults]) do
    Enum.sum adults
  end
end
