defmodule Prob do
  def probabilities(string, gcs) do
    String.split(gcs)
    |> Enum.map(&(String.to_float(&1)))
    |> Enum.map(&(calculate(string, &1)))
    |> Enum.join(" ")
  end

  defp calculate(string, gc) do
    String.split(string, "", trim: true)
    |> Enum.reduce(1, fn(n, acc) ->
      case n do
        "A" -> acc * (1 - gc) / 2
        "T" -> acc * (1 - gc) / 2
        "C" -> acc * gc / 2
        "G" -> acc * gc / 2
      end
    end)
    |> :math.log10
    |> Float.round(3)
  end
end
